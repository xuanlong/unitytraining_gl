﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputManager : MonoBehaviour
{
    #region params
    [SerializeField]
    LayerMask m_FilterMasks;
    [SerializeField]
    float m_RaycastDistance;
    #endregion

    #region delegate - events 
    public static event Action<Vector3> Evt_FireItem;
    #endregion

    #region unity methods
    // Start is called before the first frame update
    void Start()
    {
        this.m_RaycastDistance = 100f;
        if (Application.targetFrameRate != 60)
            Application.targetFrameRate = 60;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, this.m_RaycastDistance, this.m_FilterMasks))
            {
                // Debug.Log("Hit object position: " + hit.point);
                if (Evt_FireItem != null && Evt_FireItem.GetInvocationList().Length > 0)
                    Evt_FireItem(hit.point);
            }
        }
    }
    #endregion
}

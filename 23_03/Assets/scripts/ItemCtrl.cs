﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCtrl : MonoBehaviour
{
    #region transition params
    public const string TRANS_IsVanishing = "IsVanishing";
    public const string TRANS_Color = "Color";
    public const string STATE_COLOR_DEFAULT = "color_default";
    public const string STATE_BASIC_VANSIH = "vanish";
    const int Red_Value = 1;
    const int Green_Value = 2;
    const int Blue_Value = 3;
    #endregion

    #region params
    [SerializeField]
    Transform Ref_ParentTransform;
    PlayerCtrl Ref_Player;
    float m_RotateSpeed;
    Animator m_AnimCtrl;
    BoxCollider m_Collider;
    // Animator
    #endregion

    #region  unity methods
    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == Constants.TAG_PLAYER)
        {
            // Destroy(gameObject);
            // gameObject.SetActive(false);
            this.m_AnimCtrl.SetBool(TRANS_IsVanishing, true);

            if (Ref_Player == null)
                Ref_Player = other.gameObject.GetComponent<PlayerCtrl>();
            Ref_Player.StopMoving();

            // Debug.Log("OnTriggerEnter call");
            PoolManager.s_Instance.KillGameObject(Ref_ParentTransform.gameObject);
            this.CheckInit();

            // this.m_AnimCtrl.SetBool(TRANS_IsVanishing, true);
        }
    }

    private void Awake() {
        m_RotateSpeed = 60f;
    }

    #endregion

    #region private methods
    void CheckInit ()
    {
        if (this.m_AnimCtrl == null)
            this.m_AnimCtrl = GetComponent<Animator>();

        if (this.m_Collider == null)
            this.m_Collider = GetComponent<BoxCollider>();
    }
    #endregion

    #region public methods

    public void SetPosition (Vector3 pos) 
    {
        if (Ref_ParentTransform != null)
            Ref_ParentTransform.position = pos;
    }

    public void SetItemActive (bool act) 
    {
        gameObject.SetActive(act);
    }

    public Vector3 GetItemPos ()
    {
        if (Ref_ParentTransform != null)
            return Ref_ParentTransform.position;
        return Vector3.zero;
    }

    public void SetType (int type)
    {
        this.CheckInit();

        if (type > 0 && type <= Blue_Value)
            m_AnimCtrl.SetInteger(TRANS_Color, type);
        else
            m_AnimCtrl.Play(STATE_COLOR_DEFAULT, 1);
    }
    #endregion
}

﻿


public static class Constants {
    #region  tags 
    public const string TAG_PLAYER = "Player";
    public const string TAG_ITEM = "Item";
    public const string TAG_GROUND = "Ground";
    public const string TAG_OBSTACLE = "Obstacle";
    #endregion

}

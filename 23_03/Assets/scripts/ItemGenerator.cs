﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemGenerator : MonoBehaviour
{
    #region delegate
    
    #endregion

    #region params
    [SerializeField]
    GameObject Pre_Item;
    List<ItemCtrl> m_Items;
    List<GameObject> m_ActiveItems;

    public static ItemGenerator s_Instance;
    #endregion

    #region unity methods
    private void OnEnable() {
        InputManager.Evt_FireItem += GenerateItem;
        PoolManager.Evt_NotifyRemoving += RemoveObject;
    }

    private void OnDisable() {
        InputManager.Evt_FireItem -= GenerateItem;
        PoolManager.Evt_NotifyRemoving -= RemoveObject;
    }
    // Start is called before the first frame update
    void Start()
    {
        if (this.m_Items == null)
            this.m_Items = new List<ItemCtrl>();

        if (this.m_ActiveItems == null)
            this.m_ActiveItems = new List<GameObject>();

        if (s_Instance == null)
            s_Instance = this;
    }

    // Update is called once per frame
    #endregion

    #region public methods
    public bool HasActiveItem ()
    {
        // bool act = true;
        // // foreach (var item in m_Items)
        // // {
        // //     if (item.isActiveAndEnabled)
        // //     {
        // //         act = true;
        // //         break;
        // //     }
        // // }
        // act = this.m_ActiveItems.Find(i => i.activeInHierarchy);
        // this.m_ActiveItems.RemoveAll(i => !i.activeInHierarchy);
        // return act;
        return this.m_ActiveItems.Find(i => i.activeInHierarchy);
    }

    public Vector3 GetActivePos ()
    {
        Vector3 pos = Vector3.zero;
        // foreach (var item in m_Items)
        //     if (item.isActiveAndEnabled)
        //     {
        //         // Debug.Log("Get Pos inside");
        //         pos = item.GetItemPos();
        //         item.SetItemActive(true);
        //         break;
        //     }
    
        if (this.m_ActiveItems.Count > 0)
            pos = this.m_ActiveItems[0].transform.position;
        return pos;
    }
    #endregion

    #region private methods
    void RemoveObject (GameObject obj) 
    {
        if (this.m_ActiveItems.Find(o => o == obj))
            this.m_ActiveItems.Remove(obj);
    }
    void GenerateItem (Vector3 pos) 
    {
        // // Vector3 newpos = pos;
        // // newpos.y = 0;
        // pos.y = 0;
        // if (Pre_Item != null)
        // {
        //     ItemCtrl item = null;
        //     foreach (var i in m_Items)
        //         if(!i.isActiveAndEnabled)
        //         {
        //             item = i;
        //             break;
        //         }

        //     if (item == null)
        //     {
        //         GameObject obj = Instantiate(Pre_Item, transform);
        //         obj.transform.SetParent(transform);
        //         // obj.transform.position = pos;
        //         item = obj.GetComponentInChildren<ItemCtrl>();
        //         this.m_Items.Add(item);
        //     }

        //     item.SetItemActive(true);
        //     item.SetPosition(pos);
        // }
        int ratio = Random.Range(0, 4);
        GameObject obj = null;
        obj = PoolManager.s_Instance.SpawItem(pos, ratio);   
        // if (ratio == 0)
        //     obj = PoolManager.s_Instance.SpawnPurple(pos);
        // else
        //     obj = PoolManager.s_Instance.SpawnYellow(pos);
        // obj = PoolManager.s_Instance.SpawItem(pos);
        this.m_ActiveItems.Add(obj);
        // var item = obj.GetComponentInChildren<ItemCtrl>();
        // m_Items.Add(item);
    }
    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCtrl : MonoBehaviour
{
    // Start is called before the first frame update
    #region params
    Vector3 m_Target;
    Vector3 m_Direction;
    [SerializeField]
    bool m_IsMovingTarget = false;

    [SerializeField]
    float m_Speed = 0.1f;
    [SerializeField]
    float m_RotateSpeed = 90f;
    const float CONSTANT_Y = 0.5f;
    #endregion

    #region unity methods

    // Update is called once per frame
    void Update()
    {
        float delta = Time.deltaTime;

        if (!this.m_IsMovingTarget && 
            ItemGenerator.s_Instance.HasActiveItem()) 
        {
            var pos = ItemGenerator.s_Instance.GetActivePos();
            // Debug.Log("active pos: " + pos);
            this.SetTarget(pos);
            // this.m_IsMovingTarget = true;
        }

        if (this.m_IsMovingTarget)
        {
            Vector3 distance = m_Direction * this.m_Speed;
            distance.y = 0;
            transform.position = transform.position + distance;
            transform.Rotate(delta * this.m_RotateSpeed, delta * this.m_RotateSpeed, delta * this.m_RotateSpeed);
        }
    }
    #endregion

    #region public methods
    public void SetTarget (Vector3 target)
    {
        // Vector3 pos = target;
        // pos.y = transform.position.y;
        this.m_Direction = (target - transform.position);
        this.m_Direction.y = 0;
        this.m_Direction.Normalize();
        this.m_IsMovingTarget = true;
        // Debug.Log("Set Player moving");
    }

    public void StopMoving ()
    {
        this.m_IsMovingTarget = false;
        // Debug.Log("stop Player moving");
    }
    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCtrl : MonoBehaviour
{
    #region params
    [SerializeField]
    Transform Ref_ParentTransform;
    PlayerCtrl Ref_Player;
    float m_RotateSpeed;
    #endregion

    #region  unity methods
    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == Constants.TAG_PLAYER)
        {
            // Destroy(gameObject);
            // gameObject.SetActive(false);
            if (Ref_Player == null)
                Ref_Player = other.gameObject.GetComponent<PlayerCtrl>();
            Ref_Player.StopMoving();

            PoolManager.s_Instance.KillGameObject(transform.parent.gameObject);
        }
    }

    private void Awake() {
        m_RotateSpeed = 60f;
    }

    private void FixedUpdate() {
        float temp = Time.fixedDeltaTime;
        float angel = temp * m_RotateSpeed;
        transform.Rotate(angel, angel, angel, Space.World);
    }

    // private void Update() {
    //     float delta = Time.deltaTime;
    //     float angel = delta * m_RotateSpeed;
    //     transform.Rotate(angel, angel, angel, Space.World);
    // }
    #endregion

    #region public methods
    // public bool IsActive ()
    // {
    //     return gameObject.activeInHierarchy;
    // }

    public void SetPosition (Vector3 pos) 
    {
        if (Ref_ParentTransform != null)
            Ref_ParentTransform.position = pos;
    }

    public void SetItemActive (bool act) 
    {
        gameObject.SetActive(act);
    }

    public Vector3 GetItemPos ()
    {
        if (Ref_ParentTransform != null)
            return Ref_ParentTransform.position;
        return Vector3.zero;
    }
    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PoolManager : MonoBehaviour
{
    #region enum
    public enum ObjectEnum 
    {
        YellowObject,
        PurpleObject
    }
    #endregion

    #region delegate - event 
    public static event Action<GameObject> Evt_NotifyRemoving;
    #endregion

    #region params
    Dictionary<GameObject, Pool> m_Pools;
    public static PoolManager s_Instance;
    public GameObject Ref_YellowObject;
    public GameObject Ref_PurpleObject;
    #endregion

    #region unity methods
    private void Awake() {
        if (s_Instance == null)
            s_Instance = this;

        if (m_Pools == null)
        {
            m_Pools = new Dictionary<GameObject, Pool>();
            Pool[] p = GetComponentsInChildren<Pool>();
            foreach (Pool i in p)
            {
                m_Pools[i.GameObject] = i;

                if (i.Ref_ObjEnum == ObjectEnum.PurpleObject)
                    this.Ref_PurpleObject = i.GameObject;
                if (i.Ref_ObjEnum == ObjectEnum.YellowObject)
                    this.Ref_YellowObject = i.GameObject;
            }

            // Debug.Log("Pool size: " + p.Length);
        }
        
    }

    private void Start() {
        
    }
    // Start is called before the first frame update
    #endregion

    #region public methods
    public GameObject SpawnPurple (Vector3 pos)
    {
        return this.Spawn(this.Ref_PurpleObject, pos, Quaternion.identity, Vector3.one);
    }

    public GameObject SpawnYellow (Vector3 pos) 
    {
        return this.Spawn(this.Ref_YellowObject, pos, Quaternion.identity, Vector3.one);
    }
    public GameObject Spawn (GameObject pre, Vector3 pos, Quaternion rotation, Vector3 scale)
    {
        GameObject obj = null;
        if (!m_Pools.ContainsKey(pre))
            return obj;

        obj = m_Pools[pre].Spawn(pos, rotation, scale);
        return obj;
    }

    public void KillGameObject (GameObject obj) 
    {
        bool b = true;
        foreach (var pool in m_Pools)
        {
            if (pool.Value.IsResponsibleForObject(obj))
            {
                pool.Value.KillObject(obj);
                b = false;

                if (Evt_NotifyRemoving != null && Evt_NotifyRemoving.GetInvocationList().Length > 0)
                    Evt_NotifyRemoving(obj);
                break;
            }
        }

        if (b) 
            Destroy(obj);
    }
    #endregion
}

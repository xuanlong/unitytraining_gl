﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCtrl : MonoBehaviour
{
    #region animation transition
    const string TRAN_IsMoving = "IsMoving";
    const string STATE_IDLE = "idle";
    const string STATE_WALKING = "walking";
    #endregion
    // Start is called before the first frame update
    #region params
    Vector3 m_Target;
    Vector3 m_Direction;
    [SerializeField]
    bool m_IsMovingTarget = false;

    [SerializeField]
    float m_Speed = 0.1f;
    [SerializeField]
    float m_RotateSpeed = 90f;
    [SerializeField]
    Transform Ref_Parent;
    Animator m_Animator;
    const float CONSTANT_Y = 0.5f;
    public Transform ref_Head;
    public Transform ref_Tail;
    #endregion

    #region unity methods
    private void Start() {
        if (m_Animator == null)
            m_Animator = GetComponent<Animator>();

        if (m_Animator != null)
            m_Animator.SetBool(TRAN_IsMoving, false);
    }

    // Update is called once per frame
    void Update()
    {
        float delta = Time.deltaTime;

        if (!this.m_IsMovingTarget && 
            ItemGenerator.s_Instance.HasActiveItem()) 
        {
            var pos = ItemGenerator.s_Instance.GetActivePos();
            // Debug.Log("active pos: " + pos);
            this.SetTarget(pos);
            // this.m_IsMovingTarget = true;
        }

        if (this.m_IsMovingTarget && m_Animator.GetCurrentAnimatorStateInfo(0).IsName(STATE_WALKING))
        {
            //  this.m_Direction = this.m_Target - Ref_Parent.position;
            this.m_Direction = Ref_Parent.position - this.m_Target;
            this.m_Direction.Normalize();
            Quaternion quadTarget = Quaternion.LookRotation(this.m_Direction);
            Quaternion lerp = Quaternion.Slerp(Ref_Parent.rotation, quadTarget, delta);
            Ref_Parent.rotation = lerp;

            Vector3 d = ref_Head.position - ref_Tail.position;
            // float dot = Vector3.Dot(m_Direction, d);
            // if (!(dot > 0.97 && dot < 1.01f))
            // {
            //     float angle = Vector3.Angle(d, m_Direction);
            //     Ref_Parent.Rotate(0, angle, 0, Space.World);
            // }

            // Vector3 distance = m_Direction * this.m_Speed;
            d.Normalize();
            Vector3 distance = d * this.m_Speed;
            distance.y = 0;
            if (Ref_Parent != null)
            {
                // transform.position = transform.position + distance;
                // transform.Rotate(delta * this.m_RotateSpeed, delta * this.m_RotateSpeed, delta * this.m_RotateSpeed);
                Ref_Parent.position = Ref_Parent.position + distance;
            }
            
        }

        // if (this.m_IsMovingTarget != m_Animator.GetBool(TRAN_IsMoving))
        //     m_Animator.SetBool(TRAN_IsMoving, this.m_IsMovingTarget);
    }
    #endregion

    #region public methods
    public void SetTarget (Vector3 target)
    {
        // Vector3 pos = target;
        // pos.y = transform.position.y;
        this.m_Target = target;
        if (Ref_Parent != null)
            this.m_Direction = (target - Ref_Parent.position);
        this.m_Direction.y = 0;
        this.m_Direction.Normalize();
        this.m_IsMovingTarget = true;
        if (this.m_Animator.GetBool(TRAN_IsMoving) != this.m_IsMovingTarget)
            this.m_Animator.SetBool(TRAN_IsMoving, this.m_IsMovingTarget);
        // Debug.Log("Set Player moving");
    }

    public void StopMoving ()
    {
        // if (ItemGenerator.s_Instance.HasActiveItem())
        {
            this.m_IsMovingTarget = false;
            m_Animator.SetBool(TRAN_IsMoving, false);
        }
        // Debug.Log("stop Player moving");
    }
    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemAEHandler : MonoBehaviour
{
    // ItemCtrl m_Ctrl;
    #region params
    Animator m_Anim;
    Quaternion m_CachedQuaternion;
    bool m_IsStartVanish = false;
    GameObject m_Parent;

    #endregion

    #region  unity methods
    private void LateUpdate() {
        if (this.m_IsStartVanish)
        {
            transform.rotation = this.m_CachedQuaternion;
            // this.m_IsStartVanish = false;
        }
    }
    #endregion

    #region private methods
    void CheckComponents ()
    {
        if (this.m_Anim == null)
            this.m_Anim = GetComponent<Animator>();
    }
    
    void OnVanishStart ()
    {
        this.CheckComponents();
        this.m_IsStartVanish = true;
        // Debug.Log("OnVanishStart");
        // transform.rotation = this.m_CachedQuaternion;
        // this.m_IsStopRotate = false;
    }

    void OnVanishGoing ()
    {
        this.m_IsStartVanish = false;
    }

    void OnVanishExit ()
    {
        m_Anim.SetBool(ItemCtrl.TRANS_IsVanishing, false);
        transform.localScale = Vector3.one;
        if (this.m_Parent == null)
            this.m_Parent = transform.parent.gameObject;
        this.m_Parent.Kill();
        // Debug.Log("OnStateUpdate");
    }
    #endregion

    #region public methods
    public void SetQuaternion (Quaternion quat)
    {
        this.m_CachedQuaternion = quat;
        // Debug.Log("Quaternion: " + this.m_CachedQuaternion);
    }
    #endregion

}

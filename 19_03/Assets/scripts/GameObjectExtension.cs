﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtension
{
    public static void Kill (this GameObject obj) 
    {
        obj.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateVanishAnim : StateMachineBehaviour
{
    Transform m_Parent;
    Transform m_Current;
    GameObject m_GOParent;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
    //    Debug.Log("OnStateEnter");
        if (this.m_Current == null)
            this.m_Current = animator.gameObject.transform;
        if (this.m_Parent == null)
            this.m_Parent = animator.gameObject.transform.parent;
        if (this.m_GOParent == null)
            this.m_GOParent = animator.gameObject.transform.parent.gameObject;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    // override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    // {
    // //    Debug.Log("OnStateUpdate");
    //     // if (stateInfo.IsName("vanish"))
    //     // {
    //     //     // animator.SetBool(ItemCtrl.TRANS_IsVanishing, false);
    //     //     // Debug.Log("OnStateUpdate");
    //     // }
    // }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
    //    animator.SetBool(ItemCtrl.TRANS_IsVanishing, false);
        this.m_Current.localScale = Vector3.one;
        this.m_GOParent.Kill();
        // animator.gameObject.transform.localScale = localScale;
    //    animator.gameObject.transform.parent.gameObject.Kill();
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}

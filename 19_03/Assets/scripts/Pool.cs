﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    #region params
    // Stack<GameObject> m_PooledObj;
    List<GameObject> m_PooledObj;
    List<GameObject> m_AliveObj;
    public GameObject @GameObject;
    public int Ref_PoolSize;
    public PoolManager.ObjectEnum Ref_ObjEnum;
    #endregion

    #region unity methods
    private void Awake() {
        if (m_AliveObj == null)
            m_AliveObj = new List<GameObject>();

        if (m_PooledObj == null)
            m_PooledObj = new List<GameObject>();

        if (Ref_PoolSize == 0)
            Ref_PoolSize = 10;
    }
    // Start is called before the first frame update
    private void Start() {
        // if (this.m_PooledObj.Count == 0)
        //     for (int i = 0; i < Ref_PoolSize; ++i)
        //         Spawn(Vector3.zero, Quaternion.identity, Vector3.one, false);
        this.InitPool();
    }
    #endregion

    #region private methods
    void InitPool ()
    {
        if (this.m_PooledObj.Count == 0)
        {
            for (int i = 0; i < Ref_PoolSize; ++i)
            {
                GameObject obj = Instantiate(GameObject, Vector3.zero, Quaternion.identity, transform);
                obj.SetActive(false);
                this.m_PooledObj.Add(obj);
            }
        }
    }
    #endregion

    #region public methods
    public bool IsResponsibleForObject (GameObject obj)
    {
        return m_AliveObj.Find(o => o == obj);
    }

    public void KillObject (GameObject obj)
    {
        int index = m_AliveObj.FindIndex(o => o == obj);
        if (index != -1)
        {
            m_AliveObj.RemoveAt(index);
            // obj.SetActive(false);
            m_PooledObj.Add(obj);
        }
    }

    public GameObject Spawn (Vector3 pos, Quaternion rotation, Vector3 scale, bool act = true)
    {
        GameObject obj = null;
        if (m_PooledObj.Count == 0)
        {
            obj = Instantiate (@GameObject, pos, rotation, transform);
            Debug.Log("Spawn Object");
        }
        else
        {
            obj = m_PooledObj.Find(o => !o.activeInHierarchy);
            m_PooledObj.Remove(obj);
        }
        obj.transform.localScale = scale;
        obj.transform.position = pos;
        obj.transform.rotation = rotation;
        // if (parent != null)
        //     obj.transform.SetParent(parent);
        obj.SetActive(act);
        m_AliveObj.Add(obj);
        return obj;
    }
    #endregion
}
